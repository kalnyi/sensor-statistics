val akkaVersion = "2.6.9"
val alpakkaVersion = "2.0.0"
val akkaStream = "com.typesafe.akka" %% "akka-stream" % akkaVersion
val alpakka = "com.lightbend.akka" %% "akka-stream-alpakka-file" % alpakkaVersion

lazy val root = (project in file("."))
.settings(
  name := "Sensor Statistics",
  version := "1.0.0-SNAPSHOT",
  libraryDependencies += akkaStream,
  libraryDependencies += alpakka,
  libraryDependencies += "org.scalatest" %% "scalatest-flatspec" % "3.2.0" % "test",
  libraryDependencies += "com.github.scopt" %% "scopt" % "4.0.0-RC2"
)