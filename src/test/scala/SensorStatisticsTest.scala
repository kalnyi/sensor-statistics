import adts._
import SensorStatistics._
import org.scalatest.flatspec.AnyFlatSpec

class SensorStatisticsTest extends AnyFlatSpec {
  "addMeasurementToStatistics" must "initialize empty statistics" in {
    val actual = addMeasurementToStatistics(SensorStatistics(id = "1"), Measurement("1", Measured(10)))
    val expected = SensorStatistics("1", Measured(10), Measured(10), Measured(10), 1)
    println(HumidityValue.fromString("10"))
    assert(actual == expected)
  }

  it must "add to measurements sum and increment total measurements" in {
    val actual = addMeasurementToStatistics(SensorStatistics(id = "1", measurementsSum = Measured(5), totalMeasurements = 1), Measurement("1", Measured(10)))
    val expected = SensorStatistics(id = "1", measurementsSum = Measured(15), totalMeasurements = 2)
    assert(actual.measurementsSum == expected.measurementsSum)
    assert(actual.totalMeasurements == expected.totalMeasurements)
  }

  it must "update min value" in {
    val actual = addMeasurementToStatistics(SensorStatistics(id = "1", min = Measured(5), totalMeasurements = 1), Measurement("1", Measured(3)))
    val expected = SensorStatistics(id = "1", min = Measured(3), totalMeasurements = 2)
    assert(actual.min == expected.min)
  }

  it must "update max value" in {
    val actual = addMeasurementToStatistics(SensorStatistics(id = "1", max = Measured(5), totalMeasurements = 1), Measurement("1", Measured(10)))
    val expected = SensorStatistics(id = "1", max = Measured(10), totalMeasurements = 2)
    assert(actual.max == expected.max)
  }

  it must "not update statistics for the other sensor" in {
    val actual = addMeasurementToStatistics(SensorStatistics("1", Measured(10), Measured(10), Measured(10), 1), Measurement("2", Measured(10)))
    val expected = SensorStatistics("1", Measured(10), Measured(10), Measured(10), 1)
    assert(actual == expected)
  }

  "avgGreaterThen" must "be true when first statistics avg is greater then second" in {
    assert(avgGreaterThen(SensorStatistics(measurementsSum = Measured(10), totalMeasurements = 1),
      SensorStatistics(measurementsSum = Measured(5), totalMeasurements = 1)))
  }

  it must "be false when first statistics avg is less then second" in {
    assert(!avgGreaterThen(SensorStatistics(measurementsSum = Measured(10), totalMeasurements = 1),
      SensorStatistics(measurementsSum = Measured(15), totalMeasurements = 1)))
  }

  it must "be true when first statistics avg is measured and second is NaN" in {
    assert(avgGreaterThen(SensorStatistics(measurementsSum = Measured(10), totalMeasurements = 1),
      SensorStatistics(measurementsSum = NaN, totalMeasurements = 1)))
  }

  it must "be false when first statistics avg is NaN" in {
    assert(!avgGreaterThen(SensorStatistics(measurementsSum = NaN, totalMeasurements = 1),
      SensorStatistics(measurementsSum = NaN, totalMeasurements = 1)))
  }
}
