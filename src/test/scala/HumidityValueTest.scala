import adts.HumidityValue._
import adts._
import org.scalatest.flatspec.AnyFlatSpec

class HumidityValueTest extends AnyFlatSpec {

  "fromString" must "create Measured with value when string is a number" in {
    assert(fromString("10").get == Measured(10))
  }

  it must "create NaN when string is NaN" in {
    assert(fromString("NaN").get == NaN)
  }

  it must "return None when string is neither a number nor a NaN" in {
    assert(fromString("test").isEmpty)
  }

  "min" must "return minimum humidity value when comparing two measured values" in {
    assert(min(Measured(10), Measured(5)) == Measured(5))
  }

  it must "return measured value when comparing measured and NaN" in {
    assert(min(Measured(10), NaN) == Measured(10))
  }

  it must "return NaN when comparing two NaNs" in {
    assert(min(NaN, NaN) == NaN)
  }

  "max" must "return maximum humidity value when comparing two measured values" in {
    assert(max(Measured(10), Measured(100)) == Measured(100))
  }

  it must "return measured value when comparing measured and NaN" in {
    assert(max(Measured(10), NaN) == Measured(10))
  }

  it must "return NaN when comparing two NaNs" in {
    assert(max(NaN, NaN) == NaN)
  }

  "sum" must "return sum of two measured values" in {
    assert(sum(Measured(10), Measured(100)) == Measured(110))
  }

  it must "return value of measured humidity when adding measured and NaN" in {
    assert(sum(Measured(10), NaN) == Measured(10))
  }

  it must "return NaN when adding two NaNs" in {
    assert(sum(NaN, NaN) == NaN)
  }
}
