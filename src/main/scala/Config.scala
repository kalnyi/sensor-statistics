case class Config(directoryPath: String)

object Config {
  def empty() = Config(null)
}
