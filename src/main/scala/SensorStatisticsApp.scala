import java.io.File
import java.nio.file.{FileSystems, Path}

import akka.actor.ActorSystem
import akka.stream.alpakka.file.scaladsl.Directory
import akka.stream.scaladsl._
import akka.stream.{ActorMaterializer, ClosedShape}
import akka.util.ByteString
import adts._
import SensorStatistics._

object SensorStatisticsApp {

  val parser = new scopt.OptionParser[Config]("Sensor Statistics") {
    head("Sensor Statistics", "1.0.0-SNAPSHOT")

    opt[String]('d', "directoryPath")
      .required()
      .action((d, c) => c.copy(directoryPath = d))
      .text("A path to directory with daily measurements files")
  }

  def main(args: Array[String]): Unit = {
    parser.parse(args, Config.empty()) match {
      case Some(config) =>{
        val dir = config.directoryPath
        val directory = new File(dir)
        println(s"Num of processed files: ${directory.listFiles().length}")

        implicit val mat = ActorMaterializer
        implicit val system = ActorSystem("Sensor_Statistics_System")

        val rg = RunnableGraph.fromGraph(GraphDSL.create(countingSink, countingSink, statsSink)((_, _, _)) { implicit builder =>
          (s1, s2, s3) =>
            import GraphDSL.Implicits._

            val broadcast = builder.add(Broadcast[String](3))

            source(dir) ~> filesFromFolderToLines ~> broadcast.in
            broadcast.out(0).filter(nonHeaderLine) ~> s1
            broadcast.out(1).filter(failedMeasurement) ~> s2
            broadcast.out(2)
              .filter(nonHeaderLine)
              .via(lineToMeasurement)
              .groupBy(10, _.sensorId)
              .fold(SensorStatistics()) {addMeasurementToStatistics}
              .mergeSubstreams ~> s3

            ClosedShape
        })

        val (f1, f2, f3) = rg.run()
        implicit val ec = system.dispatcher
        for {
          r1 <- f1
          r2 <- f2
          r3 <- f3
        } {
          print(
            s"""Num of processed measurements: $r1
               |Num of failed measurements: $r2
               |
               |Sensors with highest avg humidity:
               |
               |sensor-id,min,avg,max
               |""".stripMargin)
          for (stats <- r3.sortWith(SensorStatistics.avgGreaterThen)) {
            println(stats)
          }
          system.terminate()
        }
      }
      case None => println(parser.usage)
    }
  }

  val source = (dir: String) => Directory.ls(FileSystems.getDefault.getPath(dir))
  val nonHeaderLine = (line: String) => !line.contains("sensor-id")
  val failedMeasurement = (line: String) => line.contains("NaN")
  val countingSink = Sink.fold[Int, String](0)((acc, _) => acc + 1)
  val statsSink = Sink.fold[List[SensorStatistics], SensorStatistics](Nil)((list, stats) => stats :: list)
  val lines = Framing.delimiter(ByteString(System.lineSeparator), 10000, allowTruncation = true).map(bs => bs.utf8String)
  val filesFromFolderToLines = Flow[Path].flatMapConcat(path => FileIO.fromPath(path).via(lines))
  val lineToMeasurement = Flow[String].map(s => Measurement(s.split(",")(0).trim, HumidityValue.fromString(s.split(",")(1).trim).getOrElse(NaN)))
}
