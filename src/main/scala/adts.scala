object adts {

  sealed trait HumidityValue

  final case class Measured(n: Int) extends HumidityValue {
    override def toString: String = n.toString
  }

  final case object NaN extends HumidityValue {
    override def toString: String = "NaN"
  }

  object HumidityValue {
    val number = """\d+""".r

    val fromString: String => Option[HumidityValue] = s => s match {
      case "NaN" => Some(NaN)
      case number(_*) => Some(Measured(s.toInt))
      case _ => None
    }

    val min: (HumidityValue, HumidityValue) => HumidityValue = (a, b) => a match {
      case NaN => b
      case Measured(x) => b match {
        case NaN => a
        case Measured(y) => Measured(Math.min(x, y))
      }
    }

    val max: (HumidityValue, HumidityValue) => HumidityValue = (a, b) => a match {
      case NaN => b
      case Measured(x) => b match {
        case NaN => a
        case Measured(y) => Measured(Math.max(x, y))
      }
    }

    val sum: (HumidityValue, HumidityValue) => HumidityValue = (a, b) => a match {
      case NaN => b
      case Measured(x) => b match {
        case NaN => a
        case Measured(y) => Measured(x + y)
      }
    }
  }

  case class Measurement(sensorId: String, value: HumidityValue)

  case class SensorStatistics(id: String = "", min: HumidityValue = NaN, measurementsSum: HumidityValue = NaN, max: HumidityValue = NaN, totalMeasurements: Int = 0){
    override def toString: String = s"$id,$min,$avgMeasurement,$max"
    def avgMeasurement = measurementsSum match {
      case NaN => NaN
      case Measured(n) => if(totalMeasurements != 0) Measured(n / totalMeasurements) else NaN
    }
  }

  object SensorStatistics {
    val avgGreaterThen: (SensorStatistics, SensorStatistics) => Boolean = (a, b) => {
      a.avgMeasurement match {
        case NaN => false
        case Measured(x) => b.avgMeasurement match {
          case NaN => true
          case Measured(y) => x > y
        }
      }
    }
    val addMeasurementToStatistics: (SensorStatistics, Measurement) => SensorStatistics = (stats, m) => {
      if (!stats.id.isEmpty && stats.id != m.sensorId)
        stats.copy()
      else {
        val totalValidMeasurements = m.value match {
          case Measured(_) => stats.totalMeasurements + 1
          case NaN => stats.totalMeasurements
        }
        val min = HumidityValue.min(stats.min, m.value)
        val max = HumidityValue.max(stats.max, m.value)
        val measurementsSum = HumidityValue.sum(stats.measurementsSum, m.value)
        SensorStatistics(m.sensorId, min, measurementsSum, max, totalValidMeasurements)
      }
    }
  }

}
